 
 
import axios from 'axios'

export class HttpService {

    
  async get (url, data){
    try{
        const response = await axios.get(url, {config:{
          headers:{
              'Access-Control-Allow-Origin' : '*',
              'Access-Control-Allow-Origin': 'Content-Type'
          }
        }})
        return response
    } catch(error) {
        throw error
    }
  }


  async postToApi (url, data) {
    try {
      const response = await axios.post(url, data)

      return response
    } catch (error) {
      return error
    }
  }

  async putToApi (url, data) {
    try {
      const response = await axios.put(url, data)

      return response
    } catch (error) {
      throw error
    }
  }
}