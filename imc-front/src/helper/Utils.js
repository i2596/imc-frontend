export const isNullOrEmpty = element => element === null || typeof element === 'undefined' || element === '' || (Array.isArray(element) && element.length === 0)
