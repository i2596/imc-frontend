/**
 * Interface that represents how to make validations
 */
export class Validator {
  /**
   *
   * @param {*} target - Form with the target validation attributes
   * @param {*} validation - Object that has already received some validation
   */
  constructor (target, validation = {}) {
    this._target = target
    this._validation = validation
    this._valid = true
  }

  get validation () {
    return this._validation
  }

  get target () {
    return this._target
  }

  /**
   *Invalidates the form, for the field informed
   *
   * @param {*} field
   * @param {*} message
   */
  addValidation (field, message) {
    this._valid = false
    this._validation[field] = message
  }

  isValid () {
    return this._valid
  }
}
