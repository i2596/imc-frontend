import React, { Fragment, useState } from "react";
import { calculateValues, sendImcToApi } from "./business/ImcApi";
import { InputForms, ResultForm } from "./view/ImcView";
import i18n from "./translation";
import { ImcValidator } from './business/ImcValidator';

function App() {
  const [state,setState] = useState({
    height:'',
    weight:'',
    valueCalculated:'',
    classification:''
  })

  const onChange = field => event => setState({...state, [field] : event.target.value })
  const handleCalculate = async () => {
      const {valueCalculated, classification} = await calculateValues(state.height,state.weight)
      setState({...state, valueCalculated: valueCalculated, classification: classification}) 
  }

  const handleSave = async () => {
    const validator = new ImcValidator(state)
    .validValues();

    if(validator.isValid()){
      await sendImcToApi({height: state.height, weight: state.weight})
    }
  }

  return (
  <Fragment>
    <InputForms 
      handleCalculate={handleCalculate}
      onChange={onChange}
      height={state.height}
      weight={state.weight}/>

  {state.valueCalculated && 
    <ResultForm 
      valueCalculated={state.valueCalculated}
      classification={state.classification}/>
    }

    {state.classification && <div>
      <button onClick={handleSave}>{i18n.t('glossary:labels.save_data')}</button>
    </div>}
  </Fragment>);
}

export default App;
