import i18n from 'i18next'
import ptBR from './locales/pt-BR/pt-BR'

const locales = {
  'pt-BR': ptBR
}

i18n.init({
  lng: 'pt-BR',
  ns: ['glossary'],
  fallbackLng: 'pt-BR',
  debug: false,
  resources: locales,

  interpolation: {
    formatSeparator: ',',
    format: function (value, format) {
      if (format === 'uppercase') return value.toUpperCase()
      if (format === 'lowercase') return value.toLowerCase()
      return value
    }
  }
})

export default i18n