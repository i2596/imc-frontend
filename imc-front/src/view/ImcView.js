
import { Fragment } from "react"
import i18n from "../translation"

export const InputForms = ({height, weight,onChange,handleCalculate}) => (    
        <Fragment>
            <div >
                <label> {i18n.t('glossary:labels.type_height')}: </label>
                <input onChange={onChange('height')} type={'number'} value={height} /> <br/><br/>

                <label> {i18n.t('glossary:labels.type_weight')} </label>
                <input onChange={onChange('weight')} type={'number'} value={weight}/>
                
            </div>
            <button onClick={handleCalculate}>{i18n.t('glossary:labels.calculate')}</button>
        </Fragment>
    )

export const ResultForm = ({valueCalculated,classification}) => (
    <Fragment>
        <div>
            <label>{i18n.t('glossary:labels.your_result')}: </label>
            <label>{parseFloat(valueCalculated).toFixed(2)}</label>
        </div>
        <div>
            <label>{i18n.t('glossary:labels.you_classification')} </label>
            <label>{i18n.t(`glossary:classification.${classification}`)}</label>
        </div>
    </Fragment>
)
export const ErrorLabel = message => {
    return <Fragment>
        <span style={{color:'red'}}>
            {message}
        </span>
    </Fragment>
}