import { Validator } from '../helper/Validator';
import { isNullOrEmpty } from '../helper/Utils';
export class ImcValidator extends Validator {

    validValues () {
        let valid = true;
        const{height, weight} = this.target;

        if(isNullOrEmpty(height) && isNullOrEmpty(weight)) {
            valid = false;
        }
        if(height > 600 && weight > 600){
            valid = false;
        }
        return valid;
    }
}

