import { HttpService } from "../helper/HelperService"


const PATH = 'http://localhost:8080/imc/'

export const calculateValues = async (height = 0.0, weight=0.0) => {
    const url = `${PATH}calculate/height/${height}/weight/${weight}`
    try {
        const  {data} = await new HttpService().get(url)    
        return data
    } catch (error) {
        
    }    
}

export const sendImcToApi = async (imcDto) => {
    const url = `${PATH}save`
    const {data} = await new HttpService().postToApi(url,imcDto)
    return data
}